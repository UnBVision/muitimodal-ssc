import torch
from torch import Tensor
import numpy as np

class Accuracy:
    def __init__(self, num_classes=40):
        self.qty = 0
        self.corrects = 0
        self.num_classes = num_classes

    def update(self, pred: Tensor, target: Tensor):
        pred = pred.view(pred.size(0), pred.size(1), -1)  # N,C,H,W => N,C,H*W
        pred = pred.transpose(1, 2)  # N,C,H*W => N,H*W,C
        _, pred = torch.max(pred, 2) # N,H*W,C => N,H*W

        target = target.view(target.size(0), -1)  # N,H,W => N,H*W

        idx = target < self.num_classes

        pred = pred[idx]
        target = target[idx]

        self.corrects += torch.sum(pred == target)
        self.qty += pred.size(0)

    def compute(self):
        return self.corrects/self.qty


class MIoU:
    def __init__(self, num_classes=40):

        self.intersection = np.zeros(num_classes)
        self.union = np.zeros(num_classes)+ 1e-10
        self.num_classes = num_classes

    def update(self, pred: Tensor, target: Tensor):
        pred = pred.view(pred.size(0), pred.size(1), -1)  # N,C,H,W => N,C,H*W
        pred = pred.transpose(1, 2)  # N,C,H*W => N,H*W,C
        _, pred = torch.max(pred, 2) # N,H*W,C => N,H*W

        target = target.view(target.size(0), -1)  # N,H,W => N,H*W

        idx = target < self.num_classes

        pred = pred[idx]
        target = target[idx]

        for i in range(self.num_classes):
            inter = torch.sum(pred[pred==i] == target[pred==i])
            union = pred[pred==i].size(0) + target[target==i].size(0) - inter
            self.intersection[i] += inter
            self.union[i] += union

    def compute(self):
        iou = self.intersection/self.union
        return np.mean(iou)

    def per_class_iou(self):
        iou = self.intersection/self.union
        return iou


