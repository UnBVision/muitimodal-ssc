from fnmatch import fnmatch
import os
import torch
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader
from skimage import io, transform
import scipy.io
import numpy as np
from torchvision.transforms.functional import normalize, resize, adjust_brightness, adjust_contrast,  adjust_gamma
from torchvision.transforms.functional import adjust_hue, adjust_saturation

import random
from PIL import Image


def get_file_prefixes_from_path(data_path, criteria="*.bin"):
    prefixes = []

    for path, subdirs, files in os.walk(data_path):
        for name in files:
            if fnmatch(name, criteria):
                prefixes.append(os.path.join(path, name)[:-(len(criteria)-1)])

    prefixes.sort()

    return prefixes


class Pad(object):
    """Pad image and mask to the desired size.
    Args:
      size (int) : minimum length/width.
      img_val (array) : image padding value.
      msk_vals (list of ints) : masks padding value.
    """

    def __init__(self, size, img_val, msk_val):
        assert isinstance(size, int)
        self.size = size
        self.img_val = img_val
        self.msk_val = msk_val

    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']
        h, w = rgb.shape[:2]
        h_pad = int(np.clip(((self.size - h) + 1) // 2, 0, 1e6))
        w_pad = int(np.clip(((self.size - w) + 1) // 2, 0, 1e6))
        pad = ((h_pad, h_pad), (w_pad, w_pad))
        rgb = np.stack(
            [
                np.pad(
                    rgb[:, :, c],
                    pad,
                    mode="constant",
                    constant_values=self.img_val[c],
                )
                for c in range(3)
            ],
            axis=2,
        )
        labels = np.pad(
                labels, pad, mode="constant", constant_values=self.msk_val
            )
        #print("rgb", rgb.shape)
        #print("labels", labels.shape)
        return {'rgb': rgb, 'labels': labels}


class Resize(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, factor=0.5):
        assert isinstance(factor, float)
        self.factor = factor

    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']

        r_factor = self.factor
        if r_factor < 0.747863248:
            r_factor = 0.747863248   # shorter size = 350

        h = int(labels.shape[0] * r_factor)
        w = int(labels.shape[1] * r_factor)

        rgb = Image.fromarray(rgb)
        labels = Image.fromarray(labels)

        rgb = np.array(resize(rgb, [h, w]))
        labels = np.array(resize(labels, [h, w], Image.NEAREST))

        return {'rgb': rgb, 'labels': labels}



class RandomResize(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, factor):
        assert isinstance(factor, tuple)
        assert len(factor) == 2
        self.factor = factor

    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']

        r_factor = random.uniform(self.factor[0], self.factor[1])
        h = int(labels.shape[0] * r_factor)
        w = int(labels.shape[1] * r_factor)

        rgb = Image.fromarray(rgb)
        labels = Image.fromarray(labels)

        rgb = np.array(resize(rgb, [h, w]))
        labels = np.array(resize(labels, [h, w], Image.NEAREST))

        return {'rgb': rgb, 'labels': labels}

class RandomColorJitter(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, brigtness=(0.7,1.3), contrast=(0.7,1.3), gamma=(0.7,1.3), hue=(-0.05,0.05), saturation=(0.7,1.3)):
        assert isinstance(brigtness, tuple)
        assert len(brigtness) == 2
        self.brigtness = brigtness

        assert isinstance(contrast, tuple)
        assert len(contrast) == 2
        self.contrast = contrast

        assert isinstance(gamma, tuple)
        assert len(gamma) == 2
        self.gamma = gamma

        assert isinstance(hue, tuple)
        assert len(hue) == 2
        self.hue = hue

        assert isinstance(saturation, tuple)
        assert len(saturation) == 2
        self.saturation = saturation



    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']

        functions = [
            adjust_brightness, adjust_contrast, adjust_gamma,
            adjust_hue, adjust_saturation
        ]

        ranges = [
            self.brigtness, self.contrast, self.gamma,
            self.hue, self.saturation
        ]

        im = Image.fromarray(rgb)

        for function, range in zip (functions, ranges):
            factor = random.uniform(range[0], range[1])
            im = function(im, factor)

        rgb = np.array(im)

        return {'rgb': rgb, 'labels': labels}

class RandomCrop(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']

        h, w = labels.shape[:2]
        new_h, new_w = self.output_size

        top = 0 if h <= new_h else np.random.randint(0, h - new_h)
        left = 0 if w <= new_w else np.random.randint(0, w - new_w)

        rgb = rgb[top: top + new_h,
                  left: left + new_w]

        labels = labels[top: top + new_h,
                        left: left + new_w]

        return {'rgb': rgb, 'labels': labels}

class CenterCrop(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, output_size):
        assert isinstance(output_size, (int, tuple))
        if isinstance(output_size, int):
            self.output_size = (output_size, output_size)
        else:
            assert len(output_size) == 2
            self.output_size = output_size

    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']

        h, w = labels.shape[:2]
        new_h, new_w = self.output_size

        top = (h - new_h)//2
        left = (w - new_w)//2

        rgb = rgb[top: top + new_h,
                  left: left + new_w]

        labels = labels[top: top + new_h,
                        left: left + new_w]

        return {'rgb': rgb, 'labels': labels}

class HorizontalFlip(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, prob=0.5):
        self.prob = prob

    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']

        rand = np.random.uniform(low=0.0, high=1.0, size=None)

        if rand < self.prob:
            rgb = np.flip(rgb,axis=1)
            labels = np.flip(labels,axis=1).copy()

        return {'rgb': rgb, 'labels': labels}

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        rgb = rgb.astype(np.float32).transpose((2, 0, 1))/255

        labels = labels.astype(np.uint8) - 1 # empty class 0 -> 255
        labels[labels == 255] = 40
        labels = labels.astype(np.int)
        return {'rgb': torch.from_numpy(rgb),
                'labels': torch.from_numpy(labels)}
class Normalize(object):
    """Crop randomly the image in a sample.

    Args:
        output_size (tuple or int): Desired output size. If int, square crop
            is made.
    """

    def __init__(self, mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]):
        self.mean = mean
        self.std = std

    def __call__(self, sample):
        rgb, labels = sample['rgb'], sample['labels']

        rgb = normalize(rgb, mean=self.mean, std=self.std )

        return {'rgb': rgb, 'labels': labels}


class NYUDataset(TensorDataset):
    """Face Landmarks dataset."""

    def __init__(self, file_prefixes, transf=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transf   (callable, optional): Optional transform to be applied
                on a sample.
        """

        """
        scenes_file = os.path.join(root_dir, file_name)
        with open(scenes_file,"r") as f:
            self.scenes = f.readlines()
        self.root_dir = root_dir
        """
        self.scenes = file_prefixes
        self.transf = transf

    def __len__(self):
        return len(self.scenes)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = {'rgb': self.read_rgb(self.scenes[idx]),
                  'labels': self.read_labels(self.scenes[idx])}

        if self.transf:
            sample = self.transf(sample)

        return sample

    def read_rgb(self, prefix):
        #return io.imread('{}/data/images/img_{}.png'.format(self.root_dir, prefix[:4]))
        return io.imread('{}_color.jpg'.format(prefix))

    def read_labels(self, prefix):
        #im = scipy.io.loadmat('{}/segmentation/img_{}.mat'.format(self.root_dir, prefix[:4]))['segmentation']
        im = io.imread('{}_labels40.png'.format(prefix, as_gray=True))
        return im

    def read_hha(self, prefix):
        im = io.imread('{}/data/hha/img_{}.png'.format(self.root_dir, prefix[:4]))
        return im

    """
    def read_normals(self, prefix):
        if self.resize is None:
            return cv2.cvtColor(cv2.imread(prefix + "_normals.png"), cv2.COLOR_BGR2RGB)
        else:
            return cv2.resize(cv2.cvtColor(cv2.imread(prefix + "_normals.png"), cv2.COLOR_BGR2RGB), self.resize)

    def read_xyz(self, prefix):
        if self.resize is None:
            return cv2.cvtColor(cv2.imread(prefix + "_xyz.png"), cv2.COLOR_BGR2RGB)
        else:
            return cv2.resize(cv2.cvtColor(cv2.imread(prefix + "_xyz.png"), cv2.COLOR_BGR2RGB), self.resize)
    """


class DL2Dev:
    def __init__(self, dl, dev):
        self.dl = dl
        self.dev = dev
        self.dataset = dl.dataset

    def __len__(self):
        return len(self.dl)

    def __iter__(self):
        batches = iter(self.dl)
        for b in batches:
            g = self.to_gpu(b['rgb'], b['labels'])
            try:
                n = g[1].detach().cpu().numpy()
                yield (g)
            except Exception as ex:
                print("DL2DEV", ex)

    def to_gpu(self, x, y):
        return x.to(self.dev), y.to(self.dev)
