import numpy as np
import torch
import time
import copy
from tqdm import tqdm
from utils.metrics import Accuracy, MIoU
from torch.utils.tensorboard import SummaryWriter
from utils.misc import get_run
from utils.image import decode_outputs, decode_labels
from utils.lr_schedulers import OneCycleLr
import torch.nn.functional as F

nyu_classes = ["wall", "floor", "cabinet", "bed", "chair", "sofa", "table", "door", "window", "bookshelf",
               "picture", "counter", "blinds", "desk", "shelves", "curtain", "dresser", "pillow", "mirror",
               "floor mat", "clothes", "ceiling", "books", "refridgerator", "tvs", "paper", "towel", "shower curtain",
               "box", "whiteboard", "person", "night stand", "toilet", "sink", "lamp", "bathtub", "bag",
               "other structure", "other furniture", "other prop", "empty"]


def train_model(model, device, dataloaders, criterion, optimizer, num_epochs=25, patience=50, scheduler=None, is_inception=False, suffix=None):
    run = get_run()
    time.sleep(.1)

    if suffix is None:
        model_name = "R{}_{}".format(run, type(model).__name__)
    else:
        model_name = "R{}_{}_{}".format(run, type(model).__name__, suffix)

    tb_writer = SummaryWriter(log_dir="log/{}".format(model_name))
    tb_writer.add_text("Optimizer", str(optimizer))
    tb_writer.add_text("Criterion", str(criterion))

    #if one_cycle:
    #    lr_scheduler = OneCycleLr(optimizer, lr_factors=[10, 1, .1, .01], milestones=[50, 100, 150, 1000], writer=tb_writer)

    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_miou = 0.0

    waiting = 0
    graph = False
    for epoch in range(num_epochs):
        # if one_cycle:
        #    lr_scheduler.update(epoch)

        # Each epoch has a training and validation phase
        for phase in ['train', 'valid']:
            # print(phase)
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0

            # Iterate over data.
            tqdm_desc = "{}: Epoch {}/{} Loss: {:.4f} Acc: {:.4f} MIoU: {:.4f}"
            num_samples = 0
            m_acc = Accuracy()
            m_miou = MIoU()

            #count = 0

            with tqdm(total=len(dataloaders[phase]), desc="") as pbar:
                for inputs, labels in dataloaders[phase]:

                    try:
                        l = labels.detach().cpu().numpy()
                    except Exception as ex:
                        print(ex)
                        return labels

                    #count += 1
                    #print(count)
                    #if count<190:
                    #    continue
                    if not graph:
                        tb_writer.add_graph(model, inputs)
                        graph = True

                    # inputs = inputs.to(device)
                    # labels = labels.to(device)
                    num_samples += inputs.size(0)

                    # zero the parameter gradients
                    optimizer.zero_grad()

                    # forward
                    # track history if only in train
                    with torch.set_grad_enabled(phase == 'train'):
                        # Get model outputs and calculate loss
                        # Special case for inception because in training it has an auxiliary output. In train
                        #   mode we calculate the loss by summing the final output and the auxiliary output
                        #   but in testing we only consider the final output.
                        if is_inception and phase == 'train':
                            # From https://discuss.pytorch.org/t/how-to-optimize-inception-model-with-auxiliary-classifiers/7958
                            outputs, aux_outputs = model(inputs)
                            loss1 = criterion(outputs, labels)
                            loss2 = criterion(aux_outputs, labels)
                            loss = loss1 + 0.4 * loss2
                        else:
                            outputs = F.interpolate(model(inputs), size=labels.shape[1:], mode="bilinear", align_corners=False
                                      )
                            try:
                                loss = criterion(outputs, labels)
                            except Exception as ex:
                                print(outputs.shape, labels.shape)
                                print(ex)
                                exit(-1)

                        m_acc.update(outputs, labels)
                        m_miou.update(outputs, labels)

                        # backward + optimize only if in training phase
                        if phase == 'train':
                            loss.backward()
                            optimizer.step()

                    # statistics
                    try:
                        l = loss.item()
                        running_loss += l * inputs.size(0)
                    except Exception as ex:
                        print(ex)
                        print(inputs.size(0))
                        print(inputs.shape)
                        print(outputs.shape)
                        print(labels.shape)
                        #tb_writer.add_images("labels", decode_labels(inputs, labels),
                        #             global_step=epoch, dataformats='NHWC')

                        l = outputs.detach().cpu().numpy()
                        print(np.unique(l))

                        #print(torch.sum(labels < 40))
                        return l

                    pbar.set_description(tqdm_desc.format(phase, epoch + 1, num_epochs,
                                                          running_loss / num_samples,
                                                          m_acc.compute(),
                                                          m_miou.compute()
                                                          )
                                         )

                    pbar.update()


            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            epoch_acc = m_acc.compute()
            epoch_miou = m_miou.compute()
            epoch_per_class_iou = m_miou.per_class_iou()

            tb_writer.add_scalar('Loss/{}'.format(phase), epoch_loss, epoch)
            tb_writer.add_scalar('Accuracy/{}'.format(phase), epoch_acc, epoch)
            tb_writer.add_scalar('mIoU/{}'.format(phase), epoch_miou, epoch)

            if phase == 'train':
                for i, lr in enumerate(group['lr'] for group in optimizer.param_groups):
                    tb_writer.add_scalar('LR/{}'.format(i), lr, epoch)

                if scheduler is not None:
                    scheduler.step()


            # deep copy the model
            if phase == 'valid' and epoch_miou > best_miou:
                waiting = 0
                print("mIoU improved from {:.5f} to  {:.5f}".format(best_miou,epoch_miou))
                best_epoch = epoch
                best_miou = epoch_miou
                best_per_class_iou = epoch_per_class_iou
                best_model_wts = copy.deepcopy(model.state_dict())
                torch.save(model.state_dict(), "weights/{}_EPOCH_{}".format(model_name, epoch))

                labels_im = decode_labels(inputs, labels)
                outputs_im = decode_outputs(inputs, outputs, labels)

                tb_writer.add_images("labels", labels_im,
                                     global_step=epoch, dataformats='NHWC')
                tb_writer.add_images("outputs", outputs_im,
                                     global_step=epoch, dataformats='NHWC')

            elif phase == 'valid':
                print("mIoU {:.5f} was not an improvement from {:.5f}".format(epoch_miou,best_miou))
                waiting += 1

            if phase == 'valid':
                tb_text = ""
                for i in range(40):
                    text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * epoch_per_class_iou[i])
                    tb_text += text
                    print(text, end="        ")
                    if i % 4 == 3:
                        print()
                tb_writer.add_text("Per Class IoU", tb_text, global_step=epoch)
                print()
                time.sleep(.5)

        torch.cuda.empty_cache()
        if waiting > patience:
            print("out of patience!!!")
            break

        # print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val MIoU%: {:6.1f}  Epoch: {}'.format(100 * best_miou, best_epoch))

    tb_text = ""
    for i in range(40):
        text = '{:12.12}: {:5.1f}'.format(nyu_classes[i], 100 * best_per_class_iou[i])
        tb_text += text
        print(text, end="        ")
        if i % 4 == 3:
            print()
    tb_writer.add_text("Best Per Class IoU", tb_text)
    print()

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model
