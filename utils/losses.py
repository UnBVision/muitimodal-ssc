import torch.nn as nn
from torch import Tensor
import numpy as np
import torch


def get_class_weights(dataloader, num_classes, dev):
    # get class frequencies
    freq = np.zeros((num_classes,))
    for _, labels in dataloader:
        for c in range(num_classes):
            freq[c] += torch.sum(labels == c)
    class_probs = freq / np.sum(freq)

    class_weights = np.append((1/class_probs)/np.sum(1/class_probs),0)

    print(class_weights)
    print(sum(class_weights))
    return torch.from_numpy(class_weights.astype(np.float32)).to(dev)


class CustomCrossEntropy(nn.CrossEntropyLoss):
    __constants__ = ['ignore_index', 'reduction']
    ignore_index: int

    def __init__(self, weight: Tensor = None, size_average=None, ignore_index: int = -100,
                 reduce=None, reduction: str = 'mean', num_classes=40) -> None:
        super(CustomCrossEntropy, self).__init__(weight, size_average, ignore_index, reduce, reduction)
        self.num_classes = num_classes

    def forward(self, input: Tensor, target: Tensor) -> Tensor:
        #print(input.shape)
        #print(target.shape)

        input = input.view(input.size(0), input.size(1), -1)  # N,C,H,W => N,C,H*W
        input = input.transpose(1, 2)  # N,C,H*W => N,H*W,C

        target = target.view(target.size(0), -1)  # N,H,W => N,H*W

        #print(input.shape)
        #print(target.shape)

        idx = target < self.num_classes

        input = input[idx]
        target = target[idx]

        return super(CustomCrossEntropy, self).forward(input, target)

